# Počítačová skříň

<figure>
  <a title="Sanyalox, CC BY-SA 4.0 &lt;https://creativecommons.org/licenses/by-sa/4.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:1319995119_w640_h640_sistemnyj-blok-no.webp"><img width="256" alt="1319995119 w640 h640 sistemnyj-blok-no" src="assets/images/pocitacova-skrin.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:1319995119_w640_h640_sistemnyj-blok-no.webp">Sanyalox</a>, <a href="https://creativecommons.org/licenses/by-sa/4.0">CC BY-SA 4.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>

Počítačová skříň je krabice z plechu a plastu, kterou má skoro každý strčenou pod stolem, ale někdy i na stole. Uvnitř se schovávají všechny součástky počítače. Mezi ně patří třeba:
- _základní deska_ s plošnými spoji, obvody a konektory pro zapojení ostatních součástek,
- _procesor_, který provádí výpočty,
- _operační paměť_, kde má procesor uložené právě běžící programy a výpočty,
- _pevný disk_ se soubory (fotkami, videi, dokumenty), ale i programy připravenými ke spuštění,
- _elektrický zdroj_, který je kabelem zapojený do zásuvky a menšími kablíky pak každé součástce dodává správné elektrické napětí.

Uvnitř to pak celé vypadá nějak takto:
<figure>
  <a title="DonES, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Silent_PC-Antec_P180.JPG"><img width="256" alt="Silent PC-Antec P180" src="assets/images/pocitacova-skrin-uvnitr.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Silent_PC-Antec_P180.JPG">DonES</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>

Zvenčí na skříni najdeš různé [konektory](hardware_periferni-zarizeni.md#konektory), do kterých můžeš přímo nebo přes kabel připojovat [periferní zařízení](hardware_periferni-zarizeni.md).
