# Programy

Uživatelské programy jsou další programy, které můžeš za pomoci [operačního systému](software_operacni-system.md) spouštět. Operační systém má v sobě předinstalované základní programy pro nastavení počítače, správce souborů, kalkulačku, někdy i kalendář a [webový prohlížeč](internet_webovy-prohlizec.md), a další si můžeš doinstalovat.

<!-- toc -->

## Instalace
Programy jsou na počítači uložené v [souborech](soubory.md) stejně jako fotky nebo dokumenty. Když instaluješ nějaký program, zkopírují se jeho soubory do počítače tak, aby je operační systém rovnou sám našel, případně se mu ještě řekne, kde je má hledat.

## Jak programy vznikají
Každý program vzniká z textu, kterému se říká [zdrojový kód](#zdrojový-kód). Tyto texty píší programátoři v programovacích jazycích.

### Zdrojový kód
Zdrojový kód je text, ve kterém pomocí programovacího jazyka programátor popsal, co má program dělat a jak má vypadat. Některým programovacím jazykům počítač a operační systém rozumí rovnou, z jiných jazyků se musí program před spuštěním nejdřív automaticky &bdquo;přeložit&ldquo;.

Zdrojový kód může vypadat třeba takhle:
```python
# nadefinuje, jak se sčítají dvě čísla označená jako 'x' a 'y'
def secti_cisla(x, y):
    return x + y

# využije nadefinované 'secti_cisla' pro sečtení 123 a 456
# výsledek uloží pod písmenko 'z'
z = secti_cisla(123, 456)

# napíše výsledek uložený pod písmenkem 'z', tedy 579
print(z)
```

Pokud je zdrojový kód pro daný program veřejně dostupný a každý se na něj může podívat, říká se, že je ten program [open-source](software_licence.md#open-source). Pokud je zdrojový kód programu tajný a nikdo k němu nesmí, říká se mu [proprietární](software_licence.md#proprietární-software).
