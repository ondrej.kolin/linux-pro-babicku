# Úvod do Linuxu pro babičku

Doufám, že ti příručka poslouží jako přehled, co v počítači najdeš, co je ten Linux, a jaké základní uživatelské programy v počítači očekávat a k čemu se ti mohou hodit. Seznam všech kapitol najdeš vlevo. Listovat mezi nimi můžeš také šipkami na klávesnici ⬅️ a ➡️.

## Pro ostatní návštěvníky
Tato příručka vzniká hlavně pro moji babičku jako &bdquo;co je co&ldquo; v počítači. Babička umí počítač zapnout, zvládá e-maily, trochu vyhledávat na internetu a &bdquo;skajpovat&ldquo;. Někdy ji ale zajímá, jaký je rozdíl mezi její e-mailovou schránkou a internetem, proč nejde nový monitor do počítače zapojit starým kabelem, nebo jestli při tom nepřijde o uložené fotky, a moji rodiče jí na to bohužel nedokážou vždy odpovědět.

Nechci z babičky udělat odborníka na sestavování počítačů ani programátora, ale chci jí připravit jednoduché základní informace, kterým porozumí a odpoví jí na další otázku, která ji při používání počítače napadne.

### Licence
[![licence](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-sa/4.0/)

Toto dílo podléhá licenci [Creative Commons Uveďte původ-Zachovejte licenci 4.0 Mezinárodní License](https://creativecommons.org/licenses/by-sa/4.0/).
