# Operační systém

Operační systém je první program, který uvidíš při zapínání počítače. Díky operačnímu systému pak můžeš celý počítač [ovládat](software_ovladani.md). Zobrazuje na monitoru plochu s ikonami, kurzor myši, se kterým můžeš hýbat, snímá klávesy stisknuté na klávesnici atd. Stará se o ovládání veškerého [hardwaru](hardware.md) a připojených [zařízení](hardware_periferni-zarizeni.md), ale taky spouští další [programy](software_programy.md).

Nejznámější operační systémy pro domácí počítače jsou:
- [<i class="fa fa-windows" aria-hidden="true"></i> Windows](#windows)
- [<i class="fa fa-linux" aria-hidden="true"></i> Linux](#linux)
- [<i class="fa fa-apple" aria-hidden="true"></i> Apple](#macos)

## Windows
Windows (&bdquo;windows&ldquo; angl. okna) jsou operační systém, který byl konkrétně na tvém počítači jako první. Vytváří ho firma Microsoft a nechává si za něj platit.

<figure>
  <a title="Original work: Microsoft File:Windows 8 logo and wordmark.svg: Multiple editors; see image description page This work: User:AxG, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Windows_logo_and_wordmark_-_2012.svg"><img width="256" alt="Windows logo and wordmark - 2012" src="assets/images/windows-logo.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Windows_logo_and_wordmark_-_2012.svg">Original work: MicrosoftFile:Windows 8 logo and wordmark.svg: Multiple editors; see image description pageThis work: User:AxG</a>, Public domain, via Wikimedia Commons
  </small></figcaption>
</figure>

Do Windows můžeš doinstalovat spoustu programů nebo her (některé zadarmo, jiné za peníze) a i proto jsou na domácích počítačích hodně rozšířené. Na druhou stranu na některých starších počítačích fungují hodně pomalu, a i to byl jeden z důvodů, proč jsme je v tvém počítači nahradili [Linuxem](#linux).

## Linux
Linux je operační systém, který je rozšířený hlavně na serverech (velké a výkonné počítače), ale dá se používat i na domácím počítači. Pracuje na něm spousta firem i jednotlivců, které ho chtějí používat, a kdokoliv další se může přidat. Proto má Linux hodně verzí a variant, kterým se říká _distribuce_, a každá může vypadat a fungovat trochu jinak.

<figure>
  <a title="lewing@isc.tamu.edu Larry Ewing and The GIMP, CC0, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Tux.svg"><img width="128" alt="Tux" src="assets/images/linux-tux.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Tux.svg">lewing@isc.tamu.edu Larry Ewing and The GIMP</a>, CC0, via Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="JohnDoe777, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:MATE_verze_1.8_na_Ubuntu_13.10.png"><img width="256" alt="MATE verze 1.8 na Ubuntu 13.10" src="assets/images/linux-mate-screenshot.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:MATE_verze_1.8_na_Ubuntu_13.10.png">JohnDoe777</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="Giorgosarv18 (talk · contribs), GPL &lt;https://www.gnu.org/licenses/gpl.html&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Desktop_Ubuntu_20.04.png"><img width="256" alt="Desktop Ubuntu 20.04" src="assets/images/linux-ubuntu-screenshot.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Desktop_Ubuntu_20.04.png">Giorgosarv18 (talk · contribs)</a>, <a href="https://www.gnu.org/licenses/gpl.html">GPL</a>, via Wikimedia Commons
  </small></figcaption>
</figure>

Programů a her, které fungují s Linuxem, je asi trochu méně než u [Windows](#windows). Většinou jsou ale [open-source](software_licence.md#open-source) a nemusíš za ně nic platit. Díky více verzím Linuxu není tak těžké najít některou, která funguje docela svižně i na starších nebo levných a málo výkonných počítačích.

## macOS
macOS je operační systém, který prodává firma Apple (&bdquo;apple&ldquo; angl. jablko) společně se svými počítači. Většinou jsou to notebooky ve stříbrné barvě a hodně se objevují ve filmech. Poznáš je podle loga s nakousnutým jablkem.

<figure>
  <a title="Original:  Rob Janoff, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Apple_logo_black.svg"><img width="128" alt="Apple logo black" src="assets/images/apple-logo.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Apple_logo_black.svg">Original:  Rob Janoff</a>, Public domain, via Wikimedia Commons
  </small></figcaption>
</figure>
