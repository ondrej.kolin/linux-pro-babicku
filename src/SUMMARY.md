# Obsah

[Úvod](./uvod.md)

- [Hardware](./hardware.md)
  - [Počítačová skříň](./hardware_pocitacova-skrin.md)
  - [Periferní zařízení](./hardware_periferni-zarizeni.md)

- [Software](./software.md)
  - [Operační systém](./software_operacni-system.md)
  - [Programy](./software_programy.md)
  - [Licence](./software_licence.md)
  - [Ovládání](./software_ovladani.md)

- [Soubory](./soubory.md)
  - [Obrázky, fotky a videa](./soubory_obrazky-fotky-videa.md)
  - [Dokumenty, tabulky a prezentace](./soubory_dokumenty-tabulky-prezentace.md)

- [Internet](./internet.md)
  - [Webový prohlížeč](./internet_webovy-prohlizec.md)
  - [Webové stránky](./internet_webove-stranky.md)
  - [Bezpečnost na internetu](./internet_bezpecnost.md)
  - [E-maily](./internet_e-maily.md)
  - [Volání a chat](./internet_volani-chat.md)

- [Počítačová nebezpečí](./pocitacova-nebezpeci.md)
  - [Nevyžádaná pošta](./pocitacova-nebezpeci_nevyzadana-posta.md)
  - [Viry](./pocitacova-nebezpeci_viry.md)

[Další materiály pro samostudium](./dalsi-materialy.md)
