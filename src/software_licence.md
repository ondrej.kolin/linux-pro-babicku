# Licence

Jako autor programu se musíš rozhodnout, pod jakou licencí (za jakých podmínek) ho mohou ostatní získat, používat, upravovat, nebo třeba i dál prodávat.

<!-- toc -->

## Proprietární software
Proprietární je takový program, kdy jeho [zdrojový kód](software_programy.md#zdrojový-kód) jenom jeho autor a nikdo další ho nemůže upravovat a ani zkontrolovat, co přesně program pole podle zdrojového kódu na počítači dělá. Pro tebe jako uživatele může být takový pořád zadarmo, ale často je potřeba ho koupit.

## Open-source
Open-source (&bdquo;open&ldquo; angl. otevřený, &bdquo;source&ldquo; angl. zdroj) jsou takové programy, pro které je jejich [zdrojový kód](software_programy.md#zdrojový-kód) veřejně dostupný a podmínky určené autorem dovolují komukoliv ten kód vzít, zkontrolovat, upravit a použít pro vlastní program. Příkladem je i [Linux](software_operacni-system.md#linux).

Protože je zdrojový kód těchto programů většinou veřejně na internetu, jsou pro tebe jako uživatele zadarmo. Platí se případně jen za nějaké další služby. To může být třeba pomoc firmám, které program používají při svém podnikání.
