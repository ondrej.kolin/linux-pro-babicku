# Periferní zařízení

<!-- toc -->

## Pro ovládání počítače
Abychom mohli s počítačem nějak interagovat, připojujeme do něj (bezdrátově, nebo pomocí kabelů do počítačové skříně) další zařízení. Nejpoužívanější asi jsou:
- 🖥️ _monitor_, kde vidíš spuštěné programy a jejich okna,
- ⌨️ _klávesnice_ pro psaní a ovládání počítače,
- 🖱️ _myš_ nebo _touchpad_ (taková dotyková plocha) pro ovládání kurzoru (<i class="fa fa-mouse-pointer" aria-hidden="true"></i> / <i class="fa fa-i-cursor" aria-hidden="true"></i>),
- 🎧 _sluchátka_, _reproduktory_ a _mikrofon_, aby bylo slyšet zvuky a hudbu z počítače, nebo jsme do něj mohli zvuky nahrávat,
- 📹 _webová kamera_ (také _webkamera_), když s někým telefonuješ přes internet a chceš, aby tě mohl i vidět,
- 🖨️ _tiskárna_ a _skener_ pro tisk a &bdquo;focení&ldquo; papírů a dokumentů.

<figure>
  <a title="Darkone, CC BY-SA 2.5 &lt;https://creativecommons.org/licenses/by-sa/2.5&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:3-Tasten-Maus_Microsoft.jpg"><img width="256" alt="3-Tasten-Maus Microsoft" src="assets/images/mys.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:3-Tasten-Maus_Microsoft.jpg">Darkone</a>, <a href="https://creativecommons.org/licenses/by-sa/2.5">CC BY-SA 2.5</a>, via Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="Gladiator z3, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Webcam.jpg"><img width="256" alt="Webcam" src="assets/images/webkamera.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Webcam.jpg">Gladiator z3</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="Joydeep, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:HP_LaserJet_1020_printer.jpg"><img width="256" alt="HP LaserJet 1020 printer" src="assets/images/tiskarna.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:HP_LaserJet_1020_printer.jpg">Joydeep</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>

Tohle jsou jenom příklady a takových zařízení je celá spousta. Mohou být i kombinovaná, např. monitor s reproduktory, tiskárna se skenerem, sluchátka či webkamera s mikrofonem apod.

## Paměťová média
Paměťové médium nebo také datový nosič se používá k ukládání souborů z počítače a případně jejich přenos výměnu s někým jiným. Po zapojení do počítače můžeš do nebo z paměťového média přesouvat a kopírovat fotky, videa nebo dokumenty. Médium, které je malé a dá se jednoduše k počítači připojovat a odpojovat, můžeš někomu dát a tím i všechny na něm uložené soubory.

K paměťovým médiím patří:
- 💾 _disketa_, která už se moc nepoužívá, a dneska by se na ni vešla sotva jedna fotka,
- 💿 _CD_ (&bdquo;cédéčko&ldquo;), které dneska koupíš třeba s hodinou hudby,
- 📀 _DVD_ (&bdquo;dévédéčko&ldquo; nebo &bdquo;dývýdýčko&ldquo;) vypadá stejně jako CD, ale vejde se tam celý film,
- <i class="fa fa-usb" aria-hidden="true"></i> _USB flash disk_ (&bdquo;fleška&ldquo; nebo &bdquo;USB klíčenka&ldquo;), který tvarem připomíná zapalovač, někdy se dá pověsit na klíče, a vejde se na něj různě mnoho podle konkrétního modelu,
- některé <i class="fa fa-hdd-o" aria-hidden="true"></i> _pevné disky_, které se dají zapojit do [konektoru USB](#konektory), a na ně se vejde úplně všechno, co máš v počítači.

<figure>
  <a title="Afrank99, CC BY-SA 2.0 &lt;https://creativecommons.org/licenses/by-sa/2.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:3,5%22-Diskette.jpg"><img width="256" alt="3,5&quot;-Diskette" src="assets/images/disketa.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:3,5%22-Diskette.jpg">Afrank99</a>, <a href="https://creativecommons.org/licenses/by-sa/2.0">CC BY-SA 2.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="Ubern00b, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:CD_autolev_crop.jpg"><img width="256" alt="CD autolev crop" src="assets/images/CD.jpg"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:CD_autolev_crop.jpg">Ubern00b</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>
<figure>
  <a title="Evan-Amos, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:SanDisk_Cruzer_Micro.png"><img width="256" alt="SanDisk Cruzer Micro" src="assets/images/USB-flash-disk.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:SanDisk_Cruzer_Micro.png">Evan-Amos</a>, Public domain, via Wikimedia Commons
  </small></figcaption>
</figure>

## Konektory
Diskety, cédéčka a dévédéčka musíš vkládat do speciálních zdířek (mechanik), aby je počítač přečetl. Flešku už zapojíš do univerzálního konektoru USB (většinou malý obdélníkový se značkou <i class="fa fa-usb" aria-hidden="true"></i>), který můžeš použít i pro některé pevné disky a zařízení, která jsou na této stránce zmíněná. U nich ale záleží na konkrétní modelu, protože někdy mají univerzální USB, ale někdy mají konektor jiný.
