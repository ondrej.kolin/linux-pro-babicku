# Hardware

Hardware (&bdquo;hard&ldquo; angl. tvrdý) je fyzické technické vybavení počítače. To znamená všechno, na co si můžeš rukou sáhnout nebo &bdquo;do toho kopnout&ldquo;, když počítač nefunguje podle očekávání.

Hlavní část počítače je v počítačové skříni, ke které jsou připojená periferní zařízení.

- [Počítačová skříň](hardware_pocitacova-skrin.md)
- [Periferní zařízení](hardware_periferni-zarizeni.md)
