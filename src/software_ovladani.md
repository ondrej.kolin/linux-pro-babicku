# Ovládání

Základní funkce počítače se ovládají pomocí myši a jejího kurzoru (<i class="fa fa-mouse-pointer" aria-hidden="true"></i> / <i class="fa fa-i-cursor" aria-hidden="true"></i>) a klávesnice pro psaní textu a [klávesové zkratky](#klávesové-zkratky).

<!-- toc -->

## Hlavní panel
Hlavní panel je ta lišta, která se táhne od levého dolního rohu k pravému.

Úplně vlevo na hlavním panelu je tlačítko &bdquo;Menu&ldquo; (na Windows by se jmenovalo &bdquo;Start&ldquo;), kde se schovávají všechny nainstalované [programy](software_programy.md), nastavení počítače a tlačítko pro vypnutí počítače. Vedle tlačítka &bdquo;Menu&ldquo; pak máš připravené ikony pro spuštění tvých nejpoužívanějších programů.

<!-- TODO: screenshot lišty -->

Pokud máš nějaký program spuštěný, v prostřední části panelu se zobrazuje jejich seznam. Klepnutím myší na program v seznamu se jeho okno zobrazí, nebo naopak schová, podobně jako při klepnutí na tlačítko <i class="fa fa-window-minimize" aria-hidden="true"></i> v pravém horním rohu toho [okna](#okna-programů).

<!-- TODO: screenshot seznamu oken -->

## Okna programů
Operační systém otevírá programy v oknech. Každý program má vlastní okno, a může jich mít i víc najednou. Na obrázku vidíš okno [webového prohlížeče](internet_webovy-prohlizec.md), ve kterém jsou všechna tlačítka pro jeho ovládání a ti prohlížeč zobrazil [webovou stránku](internet_webove-stranky.md), kterou jsi chtěla.

<figure>
  <a title="JohnDoe777, CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:MATE_verze_1.8_na_Ubuntu_13.10.png"><img width="256" alt="MATE verze 1.8 na Ubuntu 13.10" src="assets/images/linux-mate-screenshot.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:MATE_verze_1.8_na_Ubuntu_13.10.png">JohnDoe777</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>

<!-- TODO: vlastní screenshot plochy s otevřeným oknem -->

V pravém horním rohu, úplně na kraji okna, jsou tři tlačítka. Zleva:
- <i class="fa fa-window-minimize" aria-hidden="true"></i> při klepnutí na to první okno schováš dolů do [hlavního panelu](#hlavní-panel). Pro jeho opětovné zobrazení na něj v panelu klepni.
- <i class="fa fa-window-maximize" aria-hidden="true"></i> při klepnutí na druhé tlačítko celé okno zvětšíš tak, že vyplní celou obrazovku,
- <i class="fa fa-times" aria-hidden="true"></i> a klepnutím na to poslední tlačítko okno a případně celý program zavřeš a vypneš.

## Kopírování textu
Pokud třeba na [webové stránce](internet_webove-stranky.md) v [e-mailu](internet_e-maily.md) nebo v nějakém [dokumentu](soubory_dokumenty-tabulky-prezentace.md) uvidíš text, který bys chtěla někomu poslat nebo si uložit do jiného dokumentu, můžeš nemusíš ho opisovat, ale můžeš ho tam zkopírovat. Nebo pokud jsi už něco někam napsala, ale chtěla bys to napsat jinam.

Jako první označ text myší. Najeď kurzorem (<i class="fa fa-i-cursor" aria-hidden="true"></i>) na začátek textu, stiskni levé tlačítko na myši a přejeď kurzorem na konec textu, který chceš kopírovat, a levé tlačítko pusť. Teď můžeš pro zkopírování textu použít [klávesovou zkratku uvedenou níže](#klávesové-zkratky), nebo na označený text klepnout pravým tlačítkem myši a z nabídnutých možností vybrat &bdquo;Kopírovat&ldquo;. Někdy je tato možnost označená ikonkou <i class="fa fa-clone" aria-hidden="true"></i>.

Teď už otevři okno a najdi místo, kde chceš kopírovaný text mít. Klepni levým tlačítkem myši na místo, kam chceš text vložit, a pak můžeš opět použít [zkratku](#klávesové-zkratky), nebo na to místo klepnout pravým tlačítkem myši a z nabídnutých možností vybrat &bdquo;Vložit&ldquo;. Tahle možnost může být označená ikonkou <i class="fa fa-clipboard" aria-hidden="true"></i>.

## Klávesové zkratky
Klávesové zkratky se používají stisknutím více kláves najednou. Můžeš je zmáčknout všechny naráz nebo jednu po druhé, ale je důležité, aby byly alespoň na chvíli všechny stisknuté společně.

<figure>
  <a title="Petr Sladek (slady), CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0/&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Qwertz_cz.svg"><img width="512" alt="Qwertz cz" src="assets/images/klavesnice-qwertz.png"></a>
  <figcaption><small>
    <a href="https://commons.wikimedia.org/wiki/File:Qwertz_cz.svg">Petr Sladek (slady)</a>, <a href="https://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons
  </small></figcaption>
</figure>

- `Ctrl`+`C` zkopíruje označený text nebo vybrané soubory (kvůli tomu `C` se někdy říká &bdquo;kontrol a cizí&ldquo;),
- `Ctrl`+`V` vloží kopírovaný text nebo soubory (kvůli tomu `C` se někdy říká &bdquo;kontrol a vlastní&ldquo;),
- `Ctrl`+`A` označí celý text dokumentu nebo webové stránky, nebo vybere všechny soubory v otevřené složce,
- `Print Screen` úplně samotná uloží všechno, co se zobrazuje na obrazovce jako obrázek. Ten pak najdeš ve složce &bdquo;Obrázky&ldquo; pojmenovaný podle aktuálního data a času. Na klávesnici je někdy označená i zkráceně, třeba `PrtSc`, a bývá vpravo nahoře nebo nad šipkami.
