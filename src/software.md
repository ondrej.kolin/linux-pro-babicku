# Software

Software (&bdquo;soft&ldquo; angl. měkký nebo hebký) jsou počítačové programy. To znamená všechno, co vidíš na obrazovce, a ještě trochu víc, co není ani vidět ani si na to nemůžeš sáhnout. Pokud byl hardware takovým tělem počítače, software je jeho myslí, která ho řídí.

- [Operační systém](software_operacni-system.md)
- [Programy](software_programy.md)
- [Licence](software_licence.md)
- [Ovládání](software_ovladani.md)
