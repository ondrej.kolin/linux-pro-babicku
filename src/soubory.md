# Soubory

Soubory najdeš jako ikony přímo na ploše po zapnutí počítače, nebo ve složkách. Ikony tvých souborů vypadají většinou jako list papíru 📄 nebo malý obrázek 🖼️. Složky poznáš podle ikony podobné složce nebo šanonu na papíry 📁.

<!-- toc -->

## Co všechno je v souborech
Fotka, video, dokument i program, to všechno je na pevném disku počítače nebo jiném [paměťovém médiu](hardware_periferni-zarizeni.md#paměťová-média) uložené jako soubor.

Soubor i složku otevřeš dvojím klepnutím levého tlačítka myši, nebo označením (jedno klepnutí) a stisknutím klávesy `Enter`. Pokud je soubor programem, poklepáním ho spustíš.

## Kopírování souborů
Kopírování souborů funguje obdobně jako [kopírování textu](software_ovladani.md#kopírování-textu). Jen místo taháním myší soubor označíš jedním klepnutím levého tlačítka myši. Pokud jich chceš označit více najednou, při klepnutí na další drž stisknutou klávesu `Ctrl`, nebo označ všechny jednom pomocí klávesnice [zkratkou](software_ovladani.md#klávesové-zkratky) `Ctrl`+`A`.

<!-- TODO: screenshot správce souborů -->

Soubory jsou ale větší než pár slov, takže jejich kopírování může chvilku trvat.

## Velikost souborů
Soubory se v počítači ukládají jako jedničky a nuly. Jejich počtem se tak měří i jejich velikost. Každá jednička nebo nula se označuje jako _bit_ (angl., čti &bdquo;byt&ldquo;), a každých osm jedniček a nul (osm bitů) dává dohromady jeden _byte_ (angl., česky &bdquo;bajt&ldquo;). Před _bajt_ se pak dávají předpony _kilo_ (tisíc), _mega_ (milion), _giga_ (miliarda) atd.

To znamená, že:
- _Kilobajt_ (zkratka &bdquo;kB&ldquo;) je osm tisíc jedniček nebo nul.<br>
Desítky nebo stovky kilobajtů mají [webové stránky](internet_webove-stranky.md) o obrázky na nich.
- _Megabajt_ (zkratka &bdquo;MB&ldquo;) je osm milionů jedniček nebo nul.<br>
Jednotky megabajtů mají třeba fotky, destítky a stovky megabajtů bývají velké programy. Na disketu se vešel přibližně megabajt a půl.
- _Gigabajt_ (zkratka &bdquo;MB&ldquo;) je osm miliard jedniček nebo nul.<br>
Tak velké jednotlivé soubory většinou ani nejsou. Na CD se vejde asi tři čtvrtě gigabajtu, na DVD necelých pět gigabajtů, kapacita USB flešek a pevných disků bývá v desítkách, stovkách nebo tisících gigabajtů. Jednotky gigabajtů jsou také většinou limitem, kolik dat nám dovolí mobilní operátor měsíčně stáhnout nebo poslat přes mobilní internet v chytrém telefonu.
