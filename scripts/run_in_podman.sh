#!/bin/bash

set -e
set -o pipefail

CONTAINER_IMAGE='docker.io/library/rust:1.49.0-buster'
COMMAND=${@}

podman pull "${CONTAINER_IMAGE}"
podman run \
  --workdir "${PWD}" \
  -v "${PWD}:${PWD}:Z" \
  --rm=true \
  --entrypoint=/bin/bash \
  -p 3000:3000/tcp \
  "${CONTAINER_IMAGE}" -c "${COMMAND}"
