CARGO_INSTALL_ROOT=.cargo
export PATH := $(CARGO_INSTALL_ROOT)/bin:$(PATH)

.PHONY: prepare
prepare:
	cargo install --root "$(CARGO_INSTALL_ROOT)" mdbook mdbook-toc
	mdbook --version
	mdbook-toc --version

.PHONY: preview
preview:
	mdbook serve --open

.PHONY: build
build:
	mdbook build

.PHONY: preview_in_podman
preview_in_podman:
	bash ./scripts/run_in_podman.sh 'make prepare && make preview'

.PHONY: build_in_podman
build_in_podman:
	bash ./scripts/run_in_podman.sh 'make prepare && make build'
