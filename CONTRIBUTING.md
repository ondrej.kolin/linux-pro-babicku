## Úpravy obsahu

### Podrobnost informací
Cílem příručky je podat jen zjednodušené základy bez podrobností, nikoliv kompletně a odborně popsat každé téma a pojem, nebo se dokonce stát primárním zdrojem podrobných informací. Pro detailní informace se dá vždy odkázat na podrobný článek na Wikipedii, ale očekávaný čtenář této příručky to zřejmě neocení.

### Styl jazyka
Příručku píšu pro svoji babičku, takže i vy prosím případné texty pište jako pro svoje rodiče nebo prarodiče. Při oslovení používám druhou osobu čísla jednotného (_ty děláš_). Zároveň se ale snažím nepoužívat příčestí minulé (_ty jsi dělal/a_), aby mohl příručku číst kdokoliv jakéhokoliv pohlaví. Pokud je to potřeba, příčestí minulé lze nahradit první osobu čísla množného (_my jsme dělali_).

### Formát souborů
Obsah všech stránek je v adresáři `src` (bez dalších podadresářů) a je psaný v Markdownu. Jako jeho dobrý přehled poslouží články:
- [Mastering Markdown - GitHub Guides](https://guides.github.com/features/mastering-markdown/)
- [Basic writing and formatting syntax - User Documentation](https://help.github.com/articles/basic-writing-and-formatting-syntax/)
- [Syntax | kramdown](https://kramdown.gettalong.org/syntax.html)

### Obrázky
Obrázky vkládám z Wikimedia Commons.
1. Najdu článek na Wikipedii, ať už české nebo anglické, který na konci obsahuje odkaz na stránku Wikimedia Commons pro stejné téma.
1. Na stránce Wikimedia Commons otevřu vybraný obrázek a klepnu na modré tlačítko po podrobnosti.
1. Na stránce obrázku vedle něj klepnu na 🌍 _Use this file on the web_.
1. V otevřeném dialogu zaškrtnu vedle _Attribution:_ checkbox _HTML_, a u _Embed this file_ vyberu velikost obrázku _256px wide_.
1. Do příručky obrázek vložím podle této šablony:
```html
<figure>
  <!-- obsah pole "Embed this file" -->
  <figcaption><small>
    <!-- obsah pole "Attribution:" -->
  </small></figcaption>
</figure>
```
1. Nakonec stáhnu obrázek uvedený v atributu `<img src="...">` z pole _Embed this file_ a uložím ho pod vhodným názvem do `src/assets/images/nazev-obrazku.png`.
1. Do atributu  `<img src="...">` vložím cestu k obrázku bez `src/` na začátku (tedy jen `assets/images/nazev-obrazku.png`).

### Ikony
Pro vkládání různých ikonek preferuji [Unicode Emoji](https://emojipedia.org/), ale fungují i [Font Awesome 4](https://fontawesome.com/v4.7.0/icons/).
