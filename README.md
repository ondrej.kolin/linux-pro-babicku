# Linux pro babičku

[![pipeline status](https://gitlab.com/MikkCZ/linux-pro-babicku/badges/master/pipeline.svg)](https://gitlab.com/MikkCZ/linux-pro-babicku/-/pipelines)

Repositář pro web [linux-pro-babicku.stanke.cz](https://linux-pro-babicku.stanke.cz/). Web používá generátor statických stránek [mdBook](https://github.com/rust-lang/mdBook/).

## Úpravy obsahu
Informace o úpravách obsahu najdete v [samostatném souboru](CONTRIBUTING.md).

## Spuštění a zobrazení na vlastním počítači

### Příprava
Abyste byli schopni spustit níže uvedené příkazy přímo na vašem počítači, je nutné mít nainstalovaný [Rust a Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html). Před prvním sestavením je ještě potřeba nainstalovat potřebné závislosti v Rustu.
```
$ make prepare
```

Pokud si nechcete instalovat Rust na vlastní počítač, můžete použít [Podman](https://podman.io/) a využívat níže uvedené příkazy pro spuštění pomocí kontejneru.

### Náhled
Při úpravách vzhledu i obsahu je dobré rovnou se podívat na výsledek. Níže uvedený příkaz sestaví obsah repositáře a zpřístupní ho na lokální adrese http://localhost:3000/.
```
$ make preview
```
nebo pomocí kontejneru
```
$ make preview_in_podman
```
Příkaz stačí spustit jednou v samostatném terminálu a nechat běžet. Pokud pak ve zdrojových souborech provedete nějakou změnu, mdBook sestaví stránky znovu a stránku v prohlížeči obnoví.

## Sestavení statické verze
Pro sestavení webu slouží tento příkaz.
```
$ make build
```
nebo pomocí kontejneru
```
$ make build_in_podman
```
Statická verze stránek je vygenerovaná do adresáře `book`. Pro nasazení stačí jeho obsah nahrát na server třeba přes FTP.
